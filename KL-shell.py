#!/usr/bin/env python3

from nutils import mesh, function, cli, solver, export
from nutils.expression_v2 import Namespace
import numpy, treelog
from matplotlib.cm import ScalarMappable, get_cmap
from matplotlib.colors import LogNorm

"""
Non-linear Kirchhoff-Love shell implementation with B-spline basis functions, 
benchmarked against the isotropic hinged cylindrical roof documented in:
K.Y. Sze et al. (2004): doi.org/10.1016/j.finel.2003.11.001

By Sai C. Divi and Stein K.F. Stoter

Nutils v7.0 "hiyamugi"
"""

def main(L:float, R:float, t:float, α:float, E:float, ν:float, P:float, \
         nelems:int, degree:int, nincs:int, dL:float, psi:float ):
    '''
    Point load on roof.

    .. arguments::
        L [508]
            length of cylinder in mm
        R [2540]
            radius of cylinder
        t [12.7]
            thickness
        α [0.1]
            angle in rad
        E [3102.75]
            Young's modulus in N/mm2
        ν [0.3]
            Poisson's ratio
        P [-3000]
            load in N
        nelems [10]
            Number of elements
        degree [3]
            Polynomial degree
        nincs [31]
            Number of load step increments
        dL [0.1]
            Arc length
        psi [0.0004]
            Relative arc length weighting. Set to zero for constant increments.
    '''

    # Input parameters in Namespace
    ns = Namespace()
    ns.pi = numpy.pi
    ns.δ2 = function.eye(2)
    ns.δ3 = function.eye(3)
    ns.Є = function.levicivita(3)
    ns.R = R
    ns.L = L
    ns.t = t
    ns.α = α
    ns.E = E
    ns.ν = ν
    ns.P = P
    ns.dL = dL
    ns.psi = psi


    # =====================================
    # Construct domain with solution fields
    # =====================================

    # Reference domain
    verts = numpy.linspace(0,1,nelems+1)
    domain, geom = mesh.rectilinear([verts, verts])
    center = domain.boundary['bottom'].boundary['left']

    # Construct bases for solution and Lagrange multiplier fields 
    basis = ns.basis = domain.basis('spline',degree=degree).vector(domain.ndims+1)
    lbasis = domain.boundary['left'].basis('spline', degree=degree)
    tbasis = domain.boundary['bottom'].basis('spline', degree=degree)

    # Fields
    ns.u = function.dotarg('udofs',basis) 
    ns.uprev = function.dotarg('uprevdofs',basis) # Required for arc-length
    ns.du_i = 'u_i - uprev_i'
    ns.ql = function.dotarg('qldofs', lbasis) # Required for BC enforcement
    ns.qt = function.dotarg('qtdofs', tbasis) # Required for BC enforcement

    # Arc length variables
    ns.inc = function.Argument('inc', ()) 
    ns.incprev = function.Argument('incprev',())
    ns.dinc = 'inc - incprev'

    # ===========================
    # Shell deformation variables
    # ===========================

    # Position vector in the Cartesian reference frame
    ns.θ  = geom
    ns.define_for('θ', gradient='∇', normal='n', jacobians=('ds', 'dl'))

    # Position vector in the undeformed configuration
    ns.X_i = 'R cos(θ_0 α) δ3_i2 + R sin(θ_0 α) δ3_i0 + 0.5 L θ_1 δ3_i1'
    ns.define_for('X', jacobians=('dV', 'dS'))

    # Position vector in the deformed configuration
    ns.x_i = 'u_i + X_i' 

    # Covariant basis vectors in the undeformed frame
    ns.G_ij = '∇_j(X_i)'
    ns.CG_ij = 'G_ki G_kj' # Covariant surface metric coefficients (Gramm matrix)
    ns.A = ('Є_ilm G_l0 G_m1'@ns).normalized()  # Third basis vector: out of plane unit normal
    
    # Contravariant basis vectors in the undeformed frame
    ns.CGI = function.inverse(ns.CG) # Contravariant surface metric coefficients
    ns.Gcontra_ij = 'CGI_jk G_ik'
    
    # Cartesian basis vectors in the undeformed frame
    ns.e0_i = 'G_i0 / sqrt(G_k0 G_k0) ' # Normalize G_0
    ns.e1_i = 'Є_ilm e0_l A_m' # Cross product with out of plane normal
    ns.e_ij = 'e0_i δ2_j0 + e1_i δ2_j1 ' # Combine

    # Covariant basis vectors in the deformed frame
    ns.g_ij = '∇_j(x_i)'
    ns.cg_ij = 'g_ki g_kj' # Covariant surface metric coefficients (Gramm matrix)
    ns.aa_i = 'Є_ilm g_l0 g_m1' # Third basis vector: out of plane unit normal
    ns.a_i = 'aa_i / sqrt(aa_k aa_k)'

    # curvature tensor coefficients
    ns.CB_ij = '∇_j(G_ki) A_k'
    ns.cb_ij = '∇_j(g_ki) a_k'


    # =================
    # Strain and stress
    # =================

    # Green-Lagrange strain coefficients in undeformed contravariant-basis
    ns.ε_ij  = '(1 / 2) (cg_ij - CG_ij)' # membrane strain
    ns.κ_ij  = '(cb_ij - CB_ij)'          # bending strain (linearly increasing with height)

    # Green-Lagrange strain coefficients in the Cartesian reference frame
    ns.εbar_ij = 'ε_ab e_ki Gcontra_ka e_lj Gcontra_lb'
    ns.κbar_ij = 'κ_ab e_ki Gcontra_ka e_lj Gcontra_lb'

    # Second Piola-Kirchhoff stress coefficients in Cartesian reference
    # from a plane-stress Saint-Venant elasticity constitutive relation
    ns.nbar_ij = '(E / (1 - ν^2)) ((1 - ν) εbar_ij + ν δ2_ij εbar_kk)' 
    ns.mbar_ij = '(E / (1 - ν^2)) ((1 - ν) κbar_ij + ν δ2_ij κbar_kk)' 

    # Strain energy density
    ns.SED = '(1 / 2) (t nbar_ij εbar_ij + (1 / 12) t^3 mbar_ij κbar_ij)'


    # ================
    # Weak formulation
    # ================

    # constraints
    sqr   = domain.boundary['right'].integral('u_i u_i dl' @ns, degree=2**degree)
    sqr  += domain.boundary['left'].integral('u_0^2 dl' @ns, degree=2**degree)
    sqr  += domain.boundary['bottom'].integral('u_1^2 dl' @ns, degree=2**degree)
    cons = solver.optimize(('udofs',), sqr, droptol=1e-15)

    # internal work
    Wint  = domain.integral('SED dS' @ns, degree=2**degree)
    Wint += domain.boundary['left'].integral('ql ∇_j(u_i A_i) n_j dl' @ns, degree=2**degree) # Lagrange multiplier for left rotation
    Wint += domain.boundary['bottom'].integral('qt ∇_j(u_i A_i) n_j dl' @ns, degree=2**degree) # Lagrange multiplier for bottom rotation

    # external work
    Wext = center.integral('0.25 inc P u_2' @ns, degree=0) # Division by 4 due to 4 quadrants of the roof

    # total work
    W = Wint - Wext

    # derivative of internal work
    dWu  = W.derivative('udofs')
    dWql = W.derivative('qldofs')
    dWqt = W.derivative('qtdofs')

    # Arc length constraint
    ns.upiui = domain.integral('du_i du_i dS' @ns, degree=2**degree)
    ArcEq = 'psi^2 upiui + dinc^2 - dL^2' @ ns

    # =============================
    # Incremental load path tracing
    # =============================

    # Initial undeformed state
    args   = {'udofs'  : numpy.zeros(len(basis)), 
              'uprevdofs'  : numpy.zeros(len(basis)), 
              'qldofs' : numpy.zeros(len(lbasis)), 
              'qtdofs' : numpy.zeros(len(tbasis)), 
              'inc': 0}

    ref = numpy.loadtxt('ref_solution.dat') # reference solution
    sol = numpy.zeros([nincs+1, 2]) # simulation

    # start increment loop
    with treelog.iter.fraction('increment', range(0,nincs)) as counter:
        for count in counter:

            # solve
            inc_ = 2 * args['inc'] - args['incprev'] if count else dL # Initial guess
            u_ = 2 * args['udofs'] - args['uprevdofs'] # Initial guess
            arguments = {**args, 'udofs':u_, 'uprevdofs':args['udofs'], "inc":inc_, 'incprev':args['inc']}
            args = solver.newton(('udofs','qldofs','qtdofs','inc'), (dWu, dWql, dWqt, ArcEq), constrain=cons, arguments=arguments).solve(tol=1e-8)
            
            # Store displacement at corner node
            sol[count+1] = Λ, u = args['inc'], center.integrate('-u_2' @ ns, degree=0, arguments=args)
            treelog.user(f"Load factor: {Λ:.2f}, displacement: {u:.2f}")

            # Plot strain energy density
            bezier = domain.sample('bezier', 3)
            x, SED = bezier.eval([ns.x, ns.SED], **args)
            norm = LogNorm()
            cmap = get_cmap('jet')
            colors = cmap(norm(SED[bezier.tri].mean(1)))
            with export.mplfigure('strain_energy.png') as fig:
                ax = fig.add_subplot(1, 1, 1, projection='3d')
                for quadrant in (1,1,1), (-1,1,1), (1,-1,1), (-1,-1,1):
                    ax.plot_trisurf(*(x * quadrant).T, triangles=bezier.tri).set_fc(colors)
                fig.colorbar(ScalarMappable(cmap=cmap, norm=norm), shrink=.5, pad=.1, label='strain energy density')

            # Plot load path
            with export.mplfigure('load_path.png') as fig:
                ax = fig.subplots(1)
                ax.plot(ref[:,1], ref[:,0], 'b-', label='Ref')
                ax.plot(sol[:count+2,1], sol[:count+2,0], 'ro-', label='Sol')
                ax.legend()
                ax.grid(True)
                ax.set_xlabel('Displacement [mm]')
                ax.set_ylabel('Load factor')

    with treelog.userfile('load_path.dat', 'w') as f:
        for Λ, u in sol:
            print(Λ, u, file=f)


if __name__ == '__main__':
    cli.run(main)

